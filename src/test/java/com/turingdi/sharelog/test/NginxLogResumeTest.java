package com.turingdi.sharelog.test;

import org.junit.Test;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

/*
 * Created by leibniz on 17-1-9.
 */
public class NginxLogResumeTest {
    @Test
    public void resume() throws IOException, ParseException {
        BufferedReader inBuf = new BufferedReader(new InputStreamReader(NginxLogResumeTest.class.getClassLoader().getResourceAsStream("share.log")));
        BufferedWriter outBuf = new BufferedWriter(new FileWriter(new File("req.txt")));
        StringBuilder sbuf = new StringBuilder();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy:hh:mm:ss Z", Locale.ENGLISH);
        String line ;
        try {
            while((line =inBuf.readLine())!= null){
                sbuf.append(line).append("\n");
                String[] columns = line.split("\001");
                if(columns.length > 2) {
                    String timestamp = columns[1];//.split(" ")[0];
                    //System.out.println(sdf.parse(timestamp).getTime());
                    String reqPath = columns[2].split(" ")[1];
                    outBuf.write(reqPath + "&ts=" + sdf.parse(timestamp).getTime() + "\n");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        outBuf.flush();
        //System.out.println(sbuf);
    }
}
