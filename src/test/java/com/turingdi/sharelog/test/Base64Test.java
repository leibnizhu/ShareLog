package com.turingdi.sharelog.test;

import java.util.Random;

/*
 * Created by leibniz on 16-12-20.
 */
public class Base64Test {
    public static void main(String[] args) {
        new Base64Test().randomCharTable();
    }

    public void randomCharTable() {
        Random rand = new Random(System.currentTimeMillis());
        StringBuilder encode = new StringBuilder("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/");
        int[] decode = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63,
                52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1,
                -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
                15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1,
                -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
                41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1};
        for (int i = 0; i < 64; i++) {
            int idx1 = rand.nextInt(64);
            int idx2 = rand.nextInt(64);
            //交换encode的字符
            char char1 = encode.charAt(idx1);
            char char2 = encode.charAt(idx2);
            encode.setCharAt(idx1, char2);
            encode.setCharAt(idx2, char1);
            //交换decode的ASCII码
            for (int j = 0; j < decode.length; j++) {
                if (decode[j] == idx1) {
                    decode[j] = idx2;
                    continue;
                }
                if (decode[j] == idx2) {
                    decode[j] = idx1;
                }
            }
        }
        System.out.println(encode.toString());
        for (int aDecode : decode) System.out.print(aDecode + ",");
    }
}
