package com.turingdi.sharelog.test;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;
import org.junit.Test;

import java.io.IOException;
import java.util.Random;

/*
 * Created by leibniz on 16-12-22.
 */
public class HBaseDaoTest {
    @Test
    public void hbaseAddTest() throws IOException {
        Random rand = new Random(System.currentTimeMillis());//生成随机rowkey

        Configuration conf = HBaseConfiguration.create();
        conf.set("hbase.zookeeper.quorum", "CentOS03:2181");
        conf.set("zookeeper.znode.parent","/hbase-unsecure");

        Connection conn = ConnectionFactory.createConnection(conf);

        TableName tableName = TableName.valueOf("SHARE_CHAIN".getBytes());
        Table table = conn.getTable(tableName);

        Put put = new Put(("row"+rand.nextInt(Integer.MAX_VALUE)).getBytes());
        put.addImmutable("PARENT".getBytes(), "TRUID".getBytes(), "123123123".getBytes());
        System.out.println(put);
        table.put(put);

        table.close();
        conn.close();
    }
}
