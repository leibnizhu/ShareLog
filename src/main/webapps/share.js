/**
 * 请在window的load事件中调用shareChain.init(企业ID);
 *
 * Created by leibniz on 16-12-21
 */
(function () {
    var sc = {
        urls: {
            addShare: "http://tag.turing.asia/addsharechain",
            getTruid: "http://tag.turing.asia/gettruid"
        },
        data: {
            /**
             * 当前用户的信息
             */
            openId: undefined,
            TRUID: undefined,
            CLUID: undefined,
            /**
             * 从请求参数中获取到的上一个用户信息
             */
            reqftid: undefined,
            reqfuid: undefined,
            reqfimei: undefined,
            reqfidfa: undefined,
            reqfwid: undefined,
            /**
             * 当前页面对应企业用户在品牌联盟中的ID
             */
            campaignId:undefined
        },
        /**
         * 初始化相关方法
         */
        init: {
            /**
             * 进行所有初始化动作
             *
             * @param cid 当前页面的活动ID
             */
            initAll: function (cid) {
                sc.data.campaignId = cid;
                //初始化加密解密算法
                sc.init.initEncryption();
                //初始化父节点参数
                sc.init.initShareParam();
                //初始化各种事件触发保存分享关系
                sc.init.initEvent();
                //开始获取各种信息
                sc.api.tryGetInfos();
            },

            /**
             * 初始化加密解密的方法,以及解析字符串参数的方法,加到String的原型中
             */
            initEncryption: function(){
                //加密解密的方法
                String.prototype.tren=function(){var h="DJOA7qGmIkxoKdPFXiSE8wCeHZ1vcTbrVh3B9LfWNpUlsnu-gjYzMyt0452RQ6,a";var c,e,a;var f,d,b;var g=this;a=g.length;e=0;c="";while(e<a){f=g.charCodeAt(e++)&255;if(e===a){c+=h.charAt(f>>2);c+=h.charAt((f&3)<<4);c+="~~";break}d=g.charCodeAt(e++);if(e===a){c+=h.charAt(f>>2);c+=h.charAt(((f&3)<<4)|((d&240)>>4));c+=h.charAt((d&15)<<2);c+="~";break}b=g.charCodeAt(e++);c+=h.charAt(f>>2);c+=h.charAt(((f&3)<<4)|((d&240)>>4));c+=h.charAt(((d&15)<<2)|((b&192)>>6));c+=h.charAt(b&63)}return c};
                String.prototype.trdc=function(){var g=[-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,62,47,-1,-1,55,26,58,34,56,57,61,4,20,36,-1,-1,-1,-1,-1,-1,-1,3,35,22,0,19,15,6,24,8,1,12,37,52,40,2,14,60,59,18,29,42,32,39,16,50,25,-1,-1,-1,-1,-1,-1,63,30,28,13,23,38,48,33,17,49,9,43,7,45,11,41,5,31,44,54,46,27,21,10,53,51,-1,-1,-1,-1,-1];var e,c,b,a;var f,h,d;var j=this;h=j.length;f=0;d="";while(f<h){do{e=g[j.charCodeAt(f++)&255]}while(f<h&&e===-1);if(e===-1){break}do{c=g[j.charCodeAt(f++)&255]}while(f<h&&c===-1);if(c===-1){break}d+=String.fromCharCode((e<<2)|((c&48)>>4));do{b=j.charCodeAt(f++)&255;if(b===126){return d}b=g[b]}while(f<h&&b===-1);if(b===-1){break}d+=String.fromCharCode(((c&15)<<4)|((b&60)>>2));do{a=j.charCodeAt(f++)&255;if(a===126){return d}a=g[a]}while(f<h&&a===-1);if(a===-1){break}d+=String.fromCharCode(((b&3)<<6)|a)}return d};
                //获取字符串中的请求参数的方法,a=参数名,s=分割字符
                String.prototype.getParam=function(a,s){var b=new RegExp("(^|"+s+")"+a+"=(.+?)("+s+"|$)","i");var c=this.match(b);if(c!==null){return decodeURIComponent(c[2])}return ""};
            },

            /**
             * 从请求中获取上一个用户的信息
             */
            initShareParam: function () {
                sc.data.reqftid = sc.data.reqfuid = sc.data.reqfimei = sc.data.reqfidfa = sc.data.reqfwid = "";
                var trfr = window.location.search.substr(1).getParam("trfr", "&");
                if (trfr !== "") {
                    var detrfr = trfr.trdc();
                    sc.data.reqftid = detrfr.getParam("ftid", "&");//请求中的此前TRUID
                    sc.data.reqfuid = detrfr.getParam("fuid", "&");//请求中的此前userId
                    sc.data.reqfimei = detrfr.getParam("fimei", "&");//请求中的此前IMEI
                    sc.data.reqfidfa = detrfr.getParam("fidfa", "&");//请求中的此前IDFA
                    sc.data.reqfwid = detrfr.getParam("fwid", "&");//请求中的此前微信OpenID
                }
            },

            /**
             * 初始化跳出页面时保存分享关系的事件
             * 以及通用的Piwik事件（提交报名、交互）
             */
            initEvent: function () {
                var isIOS = !!navigator.userAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
                //返回或跳转其他页面
                if (typeof($) === 'undefined') {
                    window.onbeforeunload = sc.api.sendRelation;//Android
                    if (isIOS) window.onpagehide = sc.api.sendRelation;
                } else {
                    $(window).bind('beforeunload', sc.api.sendRelation);//Android
                    if (isIOS) $(window).on("pagehide", sc.api.sendRelation);
                }
                //关闭页面
                window.onblur = sc.api.sendRelation;

                //交互的点击事件提交Piwik
                $(".turing_interaction").on("click", function(){
                    _paq.push([ "trackEvent", "FissionSales", "interaction", sc.data.campaignId, 1]);
                    _paq.push([ "trackPageView"]);
                });
                //报名事件提交Piwik
                $(".turing_signup").on("click", function(){
                    _paq.push([ "trackEvent", "FissionSales", "sign_up", sc.data.campaignId, 1]);
                    _paq.push([ "trackPageView"]);
                });
            }
        },

        api: {
            /**
             * 获取用户所有信息
             */
            tryGetInfos: function () {
                //设置活动ID 自定义变量
                _paq.push(['setCustomVariable', 4, 'campaign_id_s', sc.data.campaignId, "visit"]);
                _paq.push(["trackPageView"]);
                //获取OpenId
                if (null !== document.getElementById("openId")) {
                    sc.data.openId = document.getElementById("openId").getAttribute("data-value");
                    _paq.push(['setCustomVariable', 5, 'wx_open_id_s', sc.data.openId, "visit"]);
                    _paq.push(["trackPageView"]);
                    sc.api.shareUrl();
                }
                //跨域获取TRUID Cookie
                setTimeout(function () {
                    sc.api.tryGetTRUID(10);
                }, 500);
                //读取Cookie获取用户商城账号
                setTimeout(function () {
                    sc.api.tryGetCLUID(10);
                }, 500);
            },

            /**
             * 跨域获取TRUID,尝试多次
             * @param times 尝试次数
             */
            tryGetTRUID: function (times) {
                if (typeof(sc.data.TRUID) === 'undefined' || sc.data.TRUID === '') {
                    $.ajax({
                        url: sc.urls.getTruid + '?domain=' + window.location.protocol + "//" + window.location.host,
                        type: 'GET',
                        crossDomain: true,
                        xhrFields: {withCredentials: true},
                        error: function (data) {
                            console.error(data);
                        },
                        success: function (data) {
                            if (data !== "") {
                                sc.data.TRUID = data;
                                sc.api.shareUrl()
                            } else {
                                if (--times > 0)
                                    setTimeout(function () {
                                        sc.api.tryGetTRUID(times);
                                    }, 1000);
                            }
                        }
                    });
                }
            },

            /**
             * 多次尝试获取用户商城账号ID
             * @param times 尝试次数
             */
            tryGetCLUID: function (times) {
                sc.data.CLUID = document.cookie.getParam("CLUID", "; ");
                if (sc.data.CLUID === "") {
                    if (--times > 0)
                        setTimeout(function () {
                            sc.api.tryGetCLUID(times);
                        }, 1000);
                } else {
                    sc.api.shareUrl();
                }
            },

            /**
             * 调用接口保存当前分享关系
             */
            sendRelation: function () {
                //获取当前请求参数
                var curUrl = window.location.href;
                //获取当前用户参数
                var curTid = sc.api.getValidString(sc.data.TRUID);//当前TRUID
                var curUid = sc.api.getValidString(sc.data.CLUID);//当前userId
                var curWid = sc.api.getValidString(sc.data.openId);//微信openid
                var campaign_id = sc.api.getValidString(sc.data.campaignId);//企业用户ID
                //记录传播链
                if (sc.data.reqfimei !== "" || sc.data.reqfidfa !== "" || !(sc.data.reqftid === curTid && sc.data.reqfuid === curUid && sc.data.reqfwid === curWid)) {//来自APP的分享，或者TRUID/userId/微信ID发生变更,imei和idfa添加不在该js中，是APP模块的。
                    //发送上下级分享关系
                    var save = new Image(1, 1);
                    save.src = sc.urls.addShare +
                        "?ptid=" + sc.data.reqftid + "&puid=" + sc.data.reqfuid + "&pimei=" + sc.data.reqfimei + "&pidfa=" + sc.data.reqfidfa + "&pwid=" + sc.data.reqfwid +
                        "&ctid=" + curTid + "&cuid=" + curUid + "&cwid=" + curWid +
                        "&cid=" + campaign_id + "&url=" + encodeURIComponent(curUrl.replace(/[&?]trfr=\w+/gi, ""));
                    var msg = "由旧用户(TRUID=" + sc.data.reqftid + ",userId=" + sc.data.reqfuid + ",openId=" + sc.data.reqfwid
                        + ")分享到新用户(TRUID=" + curTid + ",userId=" + curUid + ",openId=" + curWid + ")";
                    console.log(msg)
                }//否则，没有来源TRUID或userID，则无需处理传播链关系
            },

            /**
             * 根据最新获取到的当前用户信息,更改URL中的额trfr参数
             */
            shareUrl: function () {
                //获取当前请求参数
                var curUrl = window.location.href;
                var trfr = window.location.search.substr(1).getParam("trfr", "&");
                //获取当前用户参数
                var curTid = sc.api.getValidString(sc.data.TRUID);//当前TRUID
                var curUid = sc.api.getValidString(sc.data.CLUID);//当前userId
                var curWid = sc.api.getValidString(sc.data.openId);//微信openid
                //生成新URL
                var newtrfr = ((curTid === "" ? "" : ("ftid=" + curTid)) + (curUid === "" ? "" : ("&fuid=" + curUid)) + (curWid === "" ? "" : ("&fwid=" + curWid))).tren();
                if (newtrfr !== "") {
                    if (trfr !== "") {
                        curUrl = curUrl.replace(new RegExp(trfr, "g"), newtrfr);//旧参数替换
                    } else {//原来没有参数，新增
                        curUrl += location.search.indexOf("?") === -1 ? "?" : "&";
                        curUrl += "trfr=" + newtrfr;
                    }
                }
                //修改到历史中，转发时为新的URL
                if (!!(window.history && history.pushState)) {// 支持History API
                    history.replaceState(null, null, curUrl);
                } else {// 不支持，需要加载native.history.js
                    var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
                    g.type = 'text/javascript';
                    g.async = false;
                    g.src = "native.history.js";
                    g.onload = g.onreadystatechange = function () {
                        if (g.ready) return false;
                        if (!g.readyState || g.readyState === "loaded" || g.readyState === 'complete') {
                            g.ready = true;
                            History.replaceState(null, null, curUrl);
                        }
                    };
                    s.parentNode.insertBefore(g, s);
                }
            },

            /**
             * 获取可用的字符串,处理undefined和null等情况
             * @param val 待处理变量
             * @returns {*} 处理后的值,如果有值则返回原来的值,若为undefined等异常情况则返回空字符串
             */
            getValidString: function (val) {
                return (typeof(val) === 'undefined') ? '' : (val || '');
            }
        }
    };
    /**
     * 请在window的load事件中调用shareChain.init();
     * @type {{init: initAll}}
     */
    window.shareChain = {
        init: function (cid) {
            sc.init.initAll(cid);
        }
    };
})();
