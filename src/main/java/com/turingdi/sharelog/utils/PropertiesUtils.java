package com.turingdi.sharelog.utils;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Properties;

/**
 * 读取配置文件，对外提供获取配置属性值的接口
 * Created by leibniz on 16-12-21.
 */
public class PropertiesUtils {
    private static Properties props = new Properties();
    private static Logger LOG = Logger.getLogger(PropertiesUtils.class);

    public static String getString(String key){
        return props.getProperty(key);
    }

    public static int getInt(String key){
        return Integer.parseInt(props.getProperty(key));
    }

    public static boolean init(){
        try {
            props.load(PropertiesUtils.class.getResourceAsStream("/sys.properties"));
        } catch (IOException e) {
            LOG.error("读取配置文件sys.properties时抛出IO异常");
            return false;
        }
        return true;
    }
}
