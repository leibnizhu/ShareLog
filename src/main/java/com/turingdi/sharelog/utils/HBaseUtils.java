package com.turingdi.sharelog.utils;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Map;

/**
 * 用于获取HBase连接，创建Put对象
 * Created by leibniz on 16-12-21.
 */
public class HBaseUtils {
    private static Configuration conf;
    private static Logger LOG = Logger.getLogger(HBaseUtils.class);

    /*
     * 静态代码快加载配置文件
     */
    static {
        conf = HBaseConfiguration.create();
        conf.set("hbase.zookeeper.quorum", PropertiesUtils.getString("ZKQuorum"));
        conf.set("zookeeper.znode.parent", PropertiesUtils.getString("ZKNodeParent"));
    }

    /**
     * 获取连接对象
     *
     * @return Connection对象
     * @author Leibniz
     */
    public static Connection getConnection() {
        Connection connection = null;
        try {
            connection = ConnectionFactory.createConnection(conf);
        } catch (IOException e) {
            LOG.error("获取HBase连接时抛出IO异常" + e.getMessage());
        }
        return connection;

    }

    /**
     * 对新建一个put的方法进行封装,避免每次都要使用很冗长的Bytes.toBytes
     *
     * @param rowKey    主键
     * @param family    列族
     * @param qualifier 列名
     * @param value     值
     * @return Put 对象
     */
    @SuppressWarnings("unused")
    public static Put changeToPut(String rowKey, String family, String qualifier, String value) {
        // 参数合法性验证
        if (CommonUtils.notEmptyString(rowKey, family, qualifier, value)) {
            Put put = new Put(Bytes.toBytes(rowKey));
            put.addColumn(Bytes.toBytes(family), Bytes.toBytes(qualifier), Bytes.toBytes(value));
            return put;
        }
        return null;
    }

    /**
     * put中有多个列族或多个列名与值,使用Map是为了防止有null键或值
     *
     * @param rowKey    主键
     * @param entityMap Map<列族, Map<列名,值>>
     * @return Put对象
     */
    public static Put changeToPut(String rowKey, Map<String, Map<String, String>> entityMap) {
        if (CommonUtils.notEmptyString(rowKey)) {
            Put put = new Put(rowKey.getBytes());
            entityMap.entrySet().forEach(
                    (entry) -> entry.getValue().entrySet().forEach(
                            (entryQV) -> put.addColumn(entry.getKey().getBytes(), entryQV.getKey().getBytes(), entryQV.getValue().getBytes())
                    )
            );
            return put;
        }
        return null;
    }
}
