package com.turingdi.sharelog.dao;

import com.turingdi.sharelog.entity.ShareChain;
import com.turingdi.sharelog.utils.HBaseUtils;

/**
 * 分享链操作的dao类
 * Created by leibniz on 16-12-21.
 */
public class ShareChainDao extends BasicHBaseDao{
    private static final String TABLE_NAME = "SHARE_CHAIN";
    private static ShareChainDao INSTANCE = new ShareChainDao();
    private ShareChainDao(){
        super(TABLE_NAME);
    }
    public static ShareChainDao getInstance(){
        return INSTANCE;
    }
    public void insert(ShareChain chain) {
        if(chain.toMap().size() > 0)
            super.insert(chain);
    }
}
