package com.turingdi.sharelog.dao;

import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import com.turingdi.sharelog.entity.HBaseEntity;
import com.turingdi.sharelog.utils.HBaseUtils;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Table;
import org.apache.log4j.Logger;

/**
 * 封装常用的HBase增删改差方法，dao类继承之即可
 * Created by leibniz on 16-12-21.
 */
public class BasicHBaseDao {
    private static Logger LOG = Logger.getLogger(BasicHBaseDao.class);
    private String tableName;

    public BasicHBaseDao(String tableName) {
        this.tableName = tableName;
    }

    /**
     * 关闭资源用
     * @param resources 待关闭的资源
     */
    private void closeResources(Closeable... resources) {
        try {
            for(Closeable resource : resources)
                resource.close();
        } catch (IOException e) {
            LOG.error("关闭资源时抛出异常：" + e.getMessage());
        }
    }

    /**
     * 往hbase中录入数据，通过HBaseUtils的getConnection静态方法
     * 获取与hbase的连接，并进一步获得表实例，对表进行录入数据之后，将表关闭
     * 并调用HBaseUtils的closeConnection静态方法释放连接资源
     * 该方法用于一次录入多条数据
     *
     * @param entities put对象，里面存储要录入的数据 return type: void write time: 下午1:46:14
     */
    public void insert(List<HBaseEntity> entities) {
        Connection connection = null;
        Table table = null;
        try {
            connection = HBaseUtils.getConnection();// 获取hbase连接
            table = connection.getTable(TableName.valueOf(this.tableName));// 获取table 对象
            List<Put> puts = new ArrayList<>();
            entities.stream().map((entity) -> HBaseUtils.changeToPut(entity.getRowKey(), entity.toMap())).forEach(puts::add);
            table.put(puts);
        } catch (IOException e) {
            LOG.info("打开HBase表时抛出异常：" + e.getMessage());
        } finally {
            closeResources(table, connection);
        }
    }

    /**
     * 重载insert方法，用于录入单条数据
     */
    public void insert(HBaseEntity entity) {
        Connection connection = null;
        Table table = null;
        try {
            connection = HBaseUtils.getConnection();// 获取hbase连接
            table = connection.getTable(TableName.valueOf(this.tableName));// 获取table 对象
            Put put = HBaseUtils.changeToPut(entity.getRowKey(), entity.toMap());
            table.put(put);
        } catch (IOException e) {
            LOG.info("打开HBase表时抛出异常：" + e.getMessage());
        } finally {
            closeResources(table, connection);
        }
    }

    /**
     * 根据get获取数据,将数据封装到hashmap里, 并通过java反射机制获取泛型类的带参构造,将hashmap
     *
     * @param get hbase的get 对象
     * @param clazz   泛型
     * @return 泛型类的实例 return type: T write time: 下午5:18:22
     * 传入该带参构造,之后取得该泛型类的实例并返回
     */
    public <T> T select(Get get, Class<T> clazz) {
        Connection connection = null;
        Table table = null;
        Map<String, String> map = new HashMap<>();
        try {
            connection = HBaseUtils.getConnection();// 连接hbase
            table = connection.getTable(TableName.valueOf(this.tableName));
            Result result = table.get(get);
            if (result != null && result.rawCells().length > 0) {
                // 遍历cell，查询结果赋值到map里
                for (Cell cell : result.rawCells())
                    map.put(new String(CellUtil.cloneQualifier(cell)), new String(CellUtil.cloneValue(cell)));
                // 通过反射机制获取泛型对象的带参构造，并获得对象的实例,并返回实例
                return clazz.getConstructor(Map.class).newInstance(map);
            }
        } catch (IOException e) {
            LOG.error("打开HBase表时抛出异常：" + e.getMessage());
            return null;
        } catch (ReflectiveOperationException e) {
            LOG.error("通过反射建立实例时抛出异常：" + e.getMessage());
            return null;
        } finally {
            closeResources(table, connection);
        }
        return null;
    }

    /**
     * 判断在tableName表中是否存在某主键
     *
     * @param get Get对象
     * @return 存在该主键, 则返回true, 反之false
     */
    public boolean isRowKeyExists(Get get) {
        Connection connection = null;
        Table table = null;
        try {
            connection = HBaseUtils.getConnection();// 获取hbase连接
            table = connection.getTable(TableName.valueOf(this.tableName));
            return !table.get(get).isEmpty();
        } catch (IOException e) {
            LOG.error("打开HBase表时抛出异常：" + e.getMessage());
        } finally {
            closeResources(table, connection);
        }
        return false;
    }
}
