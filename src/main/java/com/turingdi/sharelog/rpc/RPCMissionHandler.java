package com.turingdi.sharelog.rpc;

import com.turingdi.sharelog.entity.ShareChain;
import com.turingdi.sharelog.service.ShareService;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.*;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.cookie.Cookie;
import io.netty.handler.codec.http.cookie.ServerCookieDecoder;
import io.netty.util.CharsetUtil;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Set;

import static io.netty.handler.codec.http.HttpHeaderNames.CONTENT_TYPE;
import static io.netty.handler.codec.http.HttpMethod.GET;
import static io.netty.handler.codec.http.HttpResponseStatus.*;

/**
 * 核心业务处理Handler
 * Created by leibniz on 16-12-21.
 */
public class RPCMissionHandler extends SimpleChannelInboundHandler<FullHttpRequest> {
    private static Logger LOG = Logger.getLogger(RPCMissionHandler.class);
    private ShareService shareService = ShareService.getInstance();
    //保存白色1*1 GIF图片的一个byte数组
    private static byte[] imageInByte;
    static{
        try {
            // 在内存中创建一张1*1图片
            BufferedImage image = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
            image.setRGB(0, 0, 0xFFFFFF);//设为白色，不容易被看到
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(image, "png", baos);
            baos.flush();
            imageInByte = baos.toByteArray();
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void channelRead0(ChannelHandlerContext ctx, FullHttpRequest req) throws Exception {
        if(!req.decoderResult().isSuccess()){
            sendError(ctx, BAD_REQUEST);
            return;
        }
        if(req.method() != GET){
            sendError(ctx, METHOD_NOT_ALLOWED);
            return;
        }
        String uri = req.uri();
        String paramUri = uri.substring(uri.indexOf("?") + 1);
        List<NameValuePair> params = URLEncodedUtils.parse(paramUri, Charset.forName("UTF-8"));
        if(uri.startsWith("/addsharechain?")){
            LOG.info(uri);
            shareService.addShareChain(ShareChain.fromParamList(params));
        } else if(uri.startsWith("/gettruid")){
            String refererDomain = null;
            String queryCookie = "TRUID";//需要查询的Cookie，默认获取TRUID
            for(NameValuePair pair : params){
                switch(pair.getName()){
                    case "domain": refererDomain = pair.getValue();break;
                    case "key": queryCookie = pair.getValue();break; //也可以通过key参数指定跨域获取的Cookie键
                    default:break;
                }
            }
            if(refererDomain != null){
                if(req.headers().get("Cookie") !=  null) {
                    Set<Cookie> cookies = ServerCookieDecoder.LAX.decode(req.headers().get("Cookie"));
                    for (Cookie cookie : cookies) {
                        if (queryCookie.equals(cookie.name())) {
                            LOG.info("Query cookie = " + queryCookie + ", value = " + cookie.value() + ", domain = " + refererDomain);
                            responseWithString(ctx, cookie.value(), refererDomain);
                            return;
                        }
                    }
                } else {
                    responseWithString(ctx, "", refererDomain);
                    return;
                }
            }
        }
        responseWithImage(ctx);
    }

    private void responseWithImage(ChannelHandlerContext ctx) {
        FullHttpResponse resp = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, OK);
        //配置不缓存
        resp.headers().set("Cache-Control", "no-cache");
        resp.headers().set("Pragma", "no-cache");
        resp.headers().set("Expires", "Wed, 31 Dec 1969 23:59:59 GMT");
        //响应类型
        resp.headers().set("Content-Type", "image/gif");
        resp.content().writeBytes(imageInByte);
        ctx.writeAndFlush(resp).addListener(ChannelFutureListener.CLOSE);
    }

    private void responseWithString(ChannelHandlerContext ctx, String data, String referer) {
        FullHttpResponse resp = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, OK);
        //配置不缓存
        resp.headers().set("Cache-Control", "no-cache");
        resp.headers().set("Pragma", "no-cache");
        resp.headers().set("Expires", "Wed, 31 Dec 1969 23:59:59 GMT");
        //配置跨域允许
        resp.headers().set("Access-Control-Allow-origin", referer);
        resp.headers().set("Access-Control-Allow-Methods", "GET, POST");
        resp.headers().set("Access-Control-Allow-Credentials", "true");
        //响应类型
        resp.headers().set("Content-Type", "text/html;charset=UTF-8");
        ByteBuf buf = Unpooled.copiedBuffer(new StringBuffer(data), CharsetUtil.UTF_8);
        resp.content().writeBytes(buf);
        buf.release();
        ctx.writeAndFlush(resp).addListener(ChannelFutureListener.CLOSE);
    }

    private void sendError(ChannelHandlerContext ctx, HttpResponseStatus status) {
        FullHttpResponse resp = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, status, Unpooled.copiedBuffer("Failure:" + status, CharsetUtil.UTF_8));
        resp.headers().set(CONTENT_TYPE, "text/plain; charset=UTF-8");
        ctx.writeAndFlush(resp).addListener(ChannelFutureListener.CLOSE);
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.writeAndFlush(Unpooled.EMPTY_BUFFER);//.addListener(ChannelFutureListener.CLOSE);
        super.channelReadComplete(ctx);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        LOG.error("抛出异常", cause);
        super.exceptionCaught(ctx, cause);
    }
}
