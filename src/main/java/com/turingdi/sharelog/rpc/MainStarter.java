package com.turingdi.sharelog.rpc;

import com.turingdi.sharelog.utils.PropertiesUtils;

/**
 * 整个项目的入口
 * Created by leibniz on 16-12-21.
 */
public class MainStarter {

    public static void main(String[] args){
        PropertiesUtils.init();
        new NettyRPCServer().start();
    }
}
