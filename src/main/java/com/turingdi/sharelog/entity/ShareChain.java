package com.turingdi.sharelog.entity;

import com.turingdi.sharelog.utils.CommonUtils;
import org.apache.http.NameValuePair;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 分享链的实体类，对应HBase数据库的结构
 * Created by leibniz on 16-12-21.
 */
@SuppressWarnings("unused")
public class ShareChain implements HBaseEntity {
    //传播链的父节点字段，对应列族parent
    private String pTruid;
    private String pUserId;
    private String pImei;
    private String pIdfa;
    private String pWechatId;
    private String timestamp;//创建时的时间戳
    //传播链的子节点字段，对应列族child
    private String cTruid;
    private String cUserId;
    private String cImei;
    private String cIdfa;
    private String cWechatId;
    //info列族
    private String url;
    private String campaignId;

    @Override
    public Map<String, Map<String, String>> toMap() {
        Map<String, String> parent = new HashMap<>();
        if(CommonUtils.notEmptyString(this.pTruid))
            parent.put("TRUID", this.pTruid);
        if(CommonUtils.notEmptyString(this.pUserId))
            parent.put("USER_ID", this.pUserId);
        if(CommonUtils.notEmptyString(this.pImei))
            parent.put("IMEI", this.pImei);
        if(CommonUtils.notEmptyString(this.pIdfa))
            parent.put("IDFA", this.pIdfa);
        if(CommonUtils.notEmptyString(this.pWechatId))
            parent.put("WECHAT_OPENID", this.pWechatId);

        Map<String, String> child = new HashMap<>();
        if(CommonUtils.notEmptyString(this.cTruid))
            child.put("TRUID", this.cTruid);
        if(CommonUtils.notEmptyString(this.cUserId))
            parent.put("USER_ID", this.cUserId);
        if(CommonUtils.notEmptyString(this.cImei))
            child.put("IMEI", this.cImei);
        if(CommonUtils.notEmptyString(this.cIdfa))
            child.put("IDFA", this.cIdfa);
        if(CommonUtils.notEmptyString(this.cWechatId))
            child.put("WECHAT_OPENID", this.cWechatId);

        Map<String, String> info = new HashMap<>();
        if(CommonUtils.notEmptyString(this.url))
            info.put("URL", this.url);
        if(CommonUtils.notEmptyString(this.campaignId))
            info.put("CAMPAIGN_ID", this.campaignId);

        Map<String, Map<String, String>> entity = new HashMap<>();
        if(parent.size() > 0)
            entity.put("PARENT", parent);
        if(child.size() > 0)
            entity.put("CHILD", child);
        if(info.size() > 0)
            entity.put("INFO", info);

        return entity;
    }

    @Override
    public String getRowKey() {
        return this.campaignId + "_" + this.timestamp + "_" +this.toString().hashCode();
    }

    /**
     * 静态工厂方法
     * @param params NameValuePair的List
     * @return ShareChain对象
     */
    public static ShareChain fromParamList(List<NameValuePair> params) {
        ShareChain entity = new ShareChain();
        params.forEach((pair) -> { //解析URL请求参数
            switch(pair.getName()) {
                case "ptid":entity.setpTruid(pair.getValue());break;
                case "puid":entity.setpUserId(pair.getValue());break;
                case "pimei":entity.setpImei(pair.getValue());break;
                case "pidfa":entity.setpIdfa(pair.getValue());break;
                case "pwid":entity.setpWechatId(pair.getValue());break;
                case "ctid":entity.setcTruid(pair.getValue());break;
                case "cuid":entity.setcUserId(pair.getValue());break;
                case "cimei":entity.setcImei(pair.getValue());break;
                case "cidfa":entity.setcIdfa(pair.getValue());break;
                case "cwid":entity.setcWechatId(pair.getValue());break;
                case "url":
                    try {
                        entity.setUrl(URLDecoder.decode(pair.getValue(), "UTF-8"));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    break;
                case "cid":entity.setCampaignId(pair.getValue());break;
                case "ts":entity.setTimestamp(pair.getValue());break;
                default:break;
            }
        });
        if(entity.getTimestamp() == null)entity.setTimestamp(String.valueOf(System.currentTimeMillis()));//时间戳，如果请求里面没有的话，就以当前服务器时间戳为准
        return entity;
    }

    @Override
    public String toString() {
        return  String.format("%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s",
                pTruid, pUserId, pImei, pIdfa, pWechatId,
                cTruid, cUserId, cImei, cIdfa, cWechatId, url, campaignId)
                .replaceAll("null", "");
    }

    public String getpWechatId() {
        return pWechatId;
    }

    public void setpWechatId(String pWechatId) {
        this.pWechatId = pWechatId;
    }

    public String getcWechatId() {
        return cWechatId;
    }

    public void setcWechatId(String cWechatId) {
        this.cWechatId = cWechatId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getpTruid() {
        return pTruid;
    }

    private void setpTruid(String pTruid) {
        this.pTruid = pTruid;
    }

    public String getpUserId() {
        return pUserId;
    }

    private void setpUserId(String pUserId) {
        this.pUserId = pUserId;
    }

    public String getpImei() {
        return pImei;
    }

    private void setpImei(String pImei) {
        this.pImei = pImei;
    }

    public String getpIdfa() {
        return pIdfa;
    }

    private void setpIdfa(String pIdfa) {
        this.pIdfa = pIdfa;
    }

    public String getcTruid() {
        return cTruid;
    }

    private void setcTruid(String cTruid) {
        this.cTruid = cTruid;
    }

    public String getcUserId() {
        return cUserId;
    }

    private void setcUserId(String cUserId) {
        this.cUserId = cUserId;
    }

    public String getcImei() {
        return cImei;
    }

    private void setcImei(String cImei) {
        this.cImei = cImei;
    }

    public String getcIdfa() {
        return cIdfa;
    }

    private void setcIdfa(String cIdfa) {
        this.cIdfa = cIdfa;
    }

    public String getTimestamp() {
        return timestamp;
    }

    private void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }
}
