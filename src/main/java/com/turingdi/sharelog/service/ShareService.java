package com.turingdi.sharelog.service;

import com.turingdi.sharelog.dao.ShareChainDao;
import com.turingdi.sharelog.entity.ShareChain;
import com.turingdi.sharelog.utils.HBaseUtils;
import org.apache.hadoop.hbase.client.Connection;

/**
 * 往HBase插入数据
 * Created by leibniz on 16-12-21.
 */
public class ShareService {
    private static ShareService INSTANCE = new ShareService();
    private ShareService(){}
    public static ShareService getInstance(){
        return INSTANCE;
    }
    private ShareChainDao dao = ShareChainDao.getInstance();
    public void addShareChain(ShareChain chain) {
        dao.insert(chain);
    }
}
